FROM openjdk:8-jre-alpine
ADD HelloWorld2.class HelloWorld2.class
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "HelloWorld2"]
